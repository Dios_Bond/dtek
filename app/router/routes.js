
const { Router } = require('express');

const router = Router();
const crud = require('../controller/crud')

router.get('/', (req, res) => res.send('Welcome'));

router.get('/getall', (req, res) => {
    res.send(crud.getAll())
});


router.get('/userid/:id', (req, res) => {
    res.send(crud.getAll())
});

router.get('/user/:start&:limit&:order&:field', function (req, res) {
    crud.getWithParam(req.params)
    //res.send('Param')
});

router.post('/create', (req, res) => {
    res.send('Welcome create')
    crud.create()
});

router.post('/delete/:id', (req, res) => {
    res.send('Welcome DELETE');
    crud.delete(req.params)
});

router.post('/update/:id', (req, res) => {
    res.send('Welcome Update');
    crud.update(req.params)
});


module.exports = router;
