const config = {
    nameapp: "test app",
    server: {
        port: 8080,
    },
    table: {
        user: "user",
        settings: "settings",
    },
    db: {
        namedb: "dtek_test",
        host: "localhost",
        port: "3306",
        user: "user",
        pass: "Password123",
    }
};

module.exports = config;
