
//const Sequelize = require('sequelize');


const query = require('mysql-query-promise');
const mysql = require('mysql');
const config = require('../config/config');
const tableUser = config.table.user;
const tableSettings = config.table.settings;



const connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.pass,
    database: config.db.namedb,
    port: config.db.port,
    connectionLimit : 10,
    insecureAuth: false
  })
  
  connection.connect(function(err) {
    if (err) {
      console.error('error connecting: ' + err.stack);
      return;
    }
   
    console.log('connected as id ' + connection.threadId);
  });

const crud = {
    getAll: async () => {
        await connection.query(`SELECT * from ${config.table.settings}`, (error, results, fields) => {
            if (error) throw error;
            // connected!
            console.log('results', results);
          });
    },

    getWithParam: async (start, limit, order, name) => {
      let query = {
        start: start,
        order: order,
        full_name: name
      }
        if (limit == '') {
        //    
            await connection.query(`SELECT value from ${tableSettings} WHERE 'key'=?`, ['limit'], (error, results, fields) => {
            if (error) throw error;
            setval('limit', results);
          });
        }

        if (order == '') {
          await connection.query(`SELECT * FROM ${tableSettings} WHERE 'key'=?`, ['defaultSort'], (error, results, fields) => {
            if (error) throw error;
            setval('order', results);
          });
        }
        function setval (keyname, value) {
          if (keyname == 'limit') {
              query.limit = value  
          };
          if (keyname == 'order') {
              query.order = value  
        } 
          console.log('Query', query)
      }

      await connection.query(`SELECT * FROM ${tableUser} ? LIMIT ${limit}`, [query], (error, results, fields) => {
        if (error) throw error;
        // connected!
        console.log('results with param', results);
        setval('order', results);
      });
    },

    get: async (id) => {
        let products = await query(`SELECT * FROM ${tableUser} WHERE id=?`,[Number(id)]);
        return products[0];
    },
    create: async function ({ name, login, date_birth, date_hire, date_fire, position_name }) {
        let user = {
            full_name: String(name),
            login: String(login),
            date_birth: Date(date_birth),
            date_hire: Date(date_hire),
            date_fire: Date(date_fire),
            position_name: String(position_name)
            };
        let result = await query(`INSERT INTO ${tableUser} SET ? ON DUPLICATE KEY UPDATE ?`,[user,user]);
        if (result.insertId) id = result.insertId;
        return crud.get(id);
    },
    update: async (id, user)=> {
        if (typeof user === 'object') {
            let updUser = {};
            if (user.hasOwnProperty('name')) updUser.full_name = String(user.name);
            if (user.hasOwnProperty('login')) updUser.login = String(user.login);
            if (user.hasOwnProperty('date_birth')) updUser.price = Date(user.date_birth);
            let result = await query(`UPDATE ${tableUser} SET ? WHERE id=?`,[updUser, Number(id)]);
            return result.affectedRows;
        }
    },
    delete: async (id) => {
        let result = await query(`DELETE FROM ${tableUser} WHERE id=?`,[Number(id)]);
        return result.affectedRows;
    }
};

//crud.getAll();
crud.getWithParam('', 40, '','');

module.exports = crud;
