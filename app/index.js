const express = require('express');
const bodyParser = require('body-parser');

const router = require('./router/routes');

const app = express();

const config = require('./config/config');
app.use(bodyParser.json());

app.use(router);

app.listen(config.server.port, function () {
	console.log(config.nameapp, 'run on port ', config.server.port);
});